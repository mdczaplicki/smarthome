#include <Wire.h>
#include <AM2320.h>

#include <srpc.h>
#include <log.h>
#include <eh.h>
#include <proto.h>
#include <IEEE754tools.h>
// We define our own ethernet layer
#define SUPLADEVICE_CPP
#include <SuplaDevice.h>
#include <lck.h>

//#include <WiFiClient.h>
#include <ESP8266WiFiType.h>
#include <ESP8266WiFi.h>
#include <ESP8266WiFiScan.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiServer.h>
#include <ESP8266WiFiGeneric.h>
#include <WiFiClientSecure.h>
#include <ESP8266WiFiAP.h>
#include <ESP8266WiFiSTA.h>
#include <WiFiUdp.h>

#define BRIGHTNESS_PIN 14 
#define BUTTON_PIN 16

int ButtonValue; // set up variable to hold value input from BUTTONPIN. 
int Old_ButtonValue; //set up variable to determine the previous value of ButtonValue

int LightOn = 0; // Used to determine whether the light is on or off, 1 being true = on, 0 being false = off
int Brightness = 0; //sets an initial brightness of 0 (none)
unsigned long startTime = 0; //determines begginning of button press

AM2320 am2320(&Wire);

unsigned char _brightness = 0;

void get_rgbw_value(int channelNumber, unsigned char *red, unsigned char *green, unsigned char *blue, unsigned char *color_brightness, unsigned char *brightness) {
 
  *brightness = _brightness;
  
}

void set_rgbw() {
    Serial.println(20.3195 * pow(1.0397, _brightness));
    Serial.println((_brightness * 1023) / 100);
    if (_brightness == 0) {
      analogWrite(BRIGHTNESS_PIN, 0);
    } else if (_brightness == 100) {
      analogWrite(BRIGHTNESS_PIN, 1023);
    } else {
      analogWrite(BRIGHTNESS_PIN, 20.3195 * pow(1.0397, _brightness));
    }
    
}
 
void set_rgbw_value(int channelNumber, unsigned char red, unsigned char green, unsigned char blue, unsigned char color_brightness, unsigned char brightness) {
 
    _brightness = brightness;
        
    set_rgbw();
  
}

void get_temperature_and_humidity(int channelNumber, double *temp, double *humidity) {
  Serial.println(am2320.Read());
  *temp = am2320.cTemp;
  *humidity = am2320.Humidity;
    Serial.print("Temperature: ");
    Serial.println(am2320.cTemp);
    Serial.print("Humidity: ");
    Serial.println(am2320.Humidity);    
}

WiFiClient client;

const char* ssid     = "Antena_5G_200%_mocy";
const char* password = "3130313039";

void setup() {
  Serial.begin(115200);
  pinMode(BRIGHTNESS_PIN, OUTPUT);
  pinMode(BUTTON_PIN, INPUT);
  Wire.begin();
  set_rgbw();
  delay(10);
  
  char GUID[SUPLA_GUID_SIZE] = {0x59,0x39,0x85,0xDE,0x43,0xE0,0xA7,0xE9,0x89,0x14,0xFC,0x11,0x54,0xCE,0x7F,0x93}; 
  uint8_t mac[6] = {0xA6, 0xB8, 0x99, 0x01, 0xAA, 0x10};

  SuplaDevice.addDimmer();
  SuplaDevice.addAM2302();

  SuplaDevice.begin(GUID, mac, "svr26.supla.org", 708, "3a69");

}

void loop() {
  SuplaDevice.iterate();
  ButtonValue = digitalRead(BUTTON_PIN); //digitalRead tells us if the value for this pin is HIGH or LOW
                                        //when button is pushed down digitalRead(BUTTONPIN) returns HIGH

    if((ButtonValue == HIGH) && (Old_ButtonValue == LOW)) { //a condition that asserts if ButtonValue is read as HIGH (button is pushed)
                                                            //AND was previously LOW
      LightOn = 1 - LightOn;
      startTime = millis(); // capture a time at button press
      
      delay(10); //for debouncing purposes, putting in a delay so that only one button press is registered
    }    

//    if((ButtonValue == HIGH) && (Old_ButtonValue == HIGH)){ // conditions to tell if the button is being held down
//        if((millis() - startTime) > 20){ //if the button is held for more than 20ms
//            _brightness += 10; //incrementally increase brightness
//            delay(1000); //delay so brightness doesn't change too quickly (longer the delay, longer it takes to reach max)
//            if(_brightness > 100){ //once brightness reaches 255
//                 _brightness = 0; // reset the LED back to 0
//             };
//             Serial.print("DUPA: ");
//             Serial.println(_brightness);
//            set_rgbw();
//          }
//      }
  
  Old_ButtonValue = ButtonValue; //after the above code has been executed, giving Old_ButtonValue the ButtonValue to detect the moment of button press. 

    if(LightOn == 1) { //when light clicks on
//        _brightness = 50;
    } else {
//        _brightness = 0;
    }
//      set_rgbw();
}

// Supla.org ethernet layer
int supla_arduino_tcp_read(void *buf, int count) {
    _supla_int_t size = client.available();
   
    if ( size > 0 ) {
        if ( size > count ) size = count;
        return client.read((uint8_t *)buf, size);
    };

    return -1;
};

int supla_arduino_tcp_write(void *buf, int count) {
    return client.write((const uint8_t *)buf, count);
};

bool supla_arduino_svr_connect(const char *server, int port) {
      return client.connect(server, 2015);
}

bool supla_arduino_svr_connected(void) {
      return client.connected();
}

void supla_arduino_svr_disconnect(void) {
     client.stop();
}

void supla_arduino_eth_setup(uint8_t mac[6], IPAddress *ip) {

   // Serial.println("WiFi init");
    WiFi.begin(ssid, password);

    while (WiFi.status() != WL_CONNECTED) {
        delay(500);
    //    Serial.print(".");
    }

    //Serial.print("\nlocalIP: ");
    //Serial.println(WiFi.localIP());
    //Serial.print("subnetMask: ");
    //Serial.println(WiFi.subnetMask());
    //Serial.print("gatewayIP: ");
    //Serial.println(WiFi.gatewayIP());
}


SuplaDeviceCallbacks supla_arduino_get_callbacks(void) {
          SuplaDeviceCallbacks cb;
          
          cb.tcp_read = &supla_arduino_tcp_read;
          cb.tcp_write = &supla_arduino_tcp_write;
          cb.eth_setup = &supla_arduino_eth_setup;
          cb.svr_connected = &supla_arduino_svr_connected;
          cb.svr_connect = &supla_arduino_svr_connect;
          cb.svr_disconnect = &supla_arduino_svr_disconnect;
          cb.get_temperature = NULL;
          cb.get_temperature_and_humidity = &get_temperature_and_humidity;
          cb.get_rgbw_value = &get_rgbw_value;
          cb.set_rgbw_value = &set_rgbw_value;
          
          return cb;
}
